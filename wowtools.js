/**
 * @file
 * Features additional drupal ajax commands.
 */

(function ($) {
  // Refreshes the current page.
  Drupal.ajax.prototype.commands.refreshCurrentPage = function (ajax, status) {
    setTimeout(function () {
      location.reload(true);
    }, 1500);
  };
})(jQuery, Drupal);
