<?php

/**
 * @file
 * Used to access the Battle Net API.
 */

/**
 * Fetches data from the Battle Net API.
 *
 * @param string $uri
 *   The rest uri.
 *
 * @return array
 *   The result with data or err_msg.
 */
function wowtools_api_data($uri) {
  $response = drupal_http_request($uri, array('timeout' => 5));
  $result = array('data' => array());
  if (isset($response->error)) {
    $result['error_code'] = $response->code;
    $result['error_msg'] = $response->error;
  }
  if (isset($response->data)) {
    $result['data'] = drupal_json_decode($response->data);
  }
  return _wowtools_api_data_valid($result);
}

/**
 * Checks if the returned data from Battle Net is valid.
 *
 * @param array $result
 *   The result from Battle Net with code and data.
 *
 * @return array
 *   The result with data or err_msg.
 */
function _wowtools_api_data_valid(array $result) {
  $respond = array();
  $result_data = $result['data'];

  // No error and data not empty.
  if (!isset($result['error_code']) && !empty($result_data)) {
    $respond['data'] = $result_data;
  }
  else {
    $respond['err_msg'] = t("Battle Net Error:") . ' ';
    if (isset($result_data['status'])) {
      $respond['err_msg'] .= $result_data['reason'];
    }
    elseif (isset($result_data['code'])) {
      $respond['err_msg'] .= "{$result_data['code']} {$result_data['type']} - {$result_data['detail']}";
    }
    // Take error information from http_request.
    elseif (isset($result['error_msg'])) {
      $respond['err_msg'] .= "{$result['error_code']} {$result['error_msg']} - " . t("If the error persists try again later!");
    }
    // Data has no content.
    else {
      $respond['err_msg'] .= t("The returned data is empty");
    }
  }
  return $respond;
}

/**
 * Checks if the returned data from Battle Net with php curl is valid.
 *
 * @param int $code
 *   HTTP error code.
 * @param string $data
 *   The data.
 *
 * @return array
 *   The result with data or err_msg.
 */
function wowtools_api_data_valid_curl($code, $data) {
  $result = array('data' => array());
  if ($code != 200) {
    $result['error_code'] = $code;
  }
  if (isset($data)) {
    $result['data'] = drupal_json_decode($data);
  }
  return _wowtools_api_data_valid($result);
}

/**
 * Validates the Battle Net API key.
 *
 * @param string $apikey
 *   The api key.
 *
 * @return array
 *   Result with err_msg if invalid.
 */
function wowtools_validate_apikey($apikey) {
  $options = array(
    'query' => array('apikey' => $apikey),
  );
  $url = url('https://us.api.battle.net/wow/data/character/classes', $options);
  return wowtools_api_data($url);
}

/**
 * Generates the url for a Battle Net API profile request.
 *
 * @param string $name
 *   The name e.g. guild name, character name.
 * @param array $fields
 *   Fields to use for the request e.g. members, progression.
 * @param bool $guild
 *   (optional) Default for character, otherwise flag for guild.
 * @param string $server
 *   (optional) The server.
 * @param string $region
 *   (optional) The region.
 *
 * @return string
 *   The complete url.
 */
function wowtools_api_profile($name, array $fields = array(), $guild = FALSE, $server = NULL, $region = NULL) {
  if (!isset($server)) {
    $server = variable_get('wowtools_guild_server');
  }
  $path = $guild == FALSE ? 'character/' : 'guild/';
  $path .= rawurlencode($server) . '/' . rawurlencode($name);
  return _wowtools_api_url($path, $fields, $region);
}

/**
 * Generates the whole url for a Battle Net API request.
 *
 * @param string $path
 *   The path after 'wow/'.
 * @param array $fields
 *   Fields to use for the request e.g. members, progression.
 * @param string $region
 *   (optional) The region.
 *
 * @return string
 *   The complete url.
 */
function _wowtools_api_url($path, array $fields = array(), $region = NULL) {
  $options = array(
    'query' => array('apikey' => variable_get('wowtools_api_apikey')),
  );
  if (!empty($fields)) {
    // No url encoding needed.
    $path .= '?fields=' . implode(',', $fields);
  }
  if (!isset($region)) {
    // No url encoding needed, region can only be 'eu' or 'us'.
    $region = variable_get('wowtools_guild_region');
  }
  return url('https://' . $region . '.api.battle.net/wow/' . $path, $options);
}

/**
 * Saves the member data.
 *
 * @param array $guild
 *   The guild member data from Battle Net.
 */
function wowtools_api_guildmembers(array $guild) {
  // If there are no members in the guild.
  if (!isset($guild['members'])) {
    return;
  }
  $members = array();
  foreach ($guild['members'] as $member) {
    // Selects only max lvl characters which also have achievements.
    if ($member['character']['level'] == 100 && $member['character']['achievementPoints'] != 0) {
      $members[$member['rank']][] = array(
        'name' => $member['character']['name'],
        'realm' => $member['character']['realm'],
      );
    }
  }
  ksort($members);
  variable_set('wowtools_guild_members', $members);
}
