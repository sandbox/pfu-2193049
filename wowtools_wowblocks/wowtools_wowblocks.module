<?php

/**
 * @file
 * Provides block settings.
 */

require_once dirname(__FILE__) . '/includes/wowtools_wowblocks.raidprogress.block.inc';
require_once dirname(__FILE__) . '/includes/wowtools_wowblocks.recruitment.block.inc';

define('WOWTOOLS_WOWBLOCKS_MODULE_NAME', 'WoWBlocks');
define('WOWTOOLS_WOWBLOCKS_BLOCK_RAIDPROGRESS', 'raidprogress');
define('WOWTOOLS_WOWBLOCKS_BLOCK_RECRUITMENT', 'recruitment');

/**
 * Implements hook_help().
 */
function wowtools_wowblocks_help($path, $arg) {
  switch ($path) {
    case 'admin/help#wowtools_wowblocks':
      $output = '';
      $output .= '<p>' . t('The WoWBlocks module features access to the World of Warcraft blocks.') . '</p>';
      $output .= '<h3>' . WOWTOOLS_WOWBLOCKS_MODULE_NAME . ': ' . t('Raid Progress') . '</h3>';
      $output .= '<p>' . t('All configuration options for this block you can find directly within the !wowtools-wowblocks-block-url.', array('!wowtools-wowblocks-block-url' => l(t('block configuration'), 'admin/structure/block/manage/wowtools_wowblocks/raidprogress/configure'))) . '</p>';
      $output .= '<h3>' . WOWTOOLS_WOWBLOCKS_MODULE_NAME . ': ' . t('Recruitment') . '</h3>';
      $output .= '<p>' . t('All configuration options for this block you can find directly within the !wowtools-wowblocks-block-url.', array('!wowtools-wowblocks-block-url' => l(t('block configuration'), 'admin/structure/block/manage/wowtools_wowblocks/recruitment/configure'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_block_info().
 */
function wowtools_wowblocks_block_info() {
  $blocks[WOWTOOLS_WOWBLOCKS_BLOCK_RAIDPROGRESS] = array(
    'info' => WOWTOOLS_WOWBLOCKS_MODULE_NAME . ': ' . t('Raid Progress'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  $blocks[WOWTOOLS_WOWBLOCKS_BLOCK_RECRUITMENT] = array(
    'info' => WOWTOOLS_WOWBLOCKS_MODULE_NAME . ': ' . t('Recruitment'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 *
 * This hook declares configuration options for blocks provided by this module.
 */
function wowtools_wowblocks_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case WOWTOOLS_WOWBLOCKS_BLOCK_RAIDPROGRESS:
      wowtools_wowblocks_raidprogress_configure($form);
      break;

    case WOWTOOLS_WOWBLOCKS_BLOCK_RECRUITMENT:
      wowtools_wowblocks_recruitment_configure($form);
      break;

  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function wowtools_wowblocks_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case WOWTOOLS_WOWBLOCKS_BLOCK_RAIDPROGRESS:
      _wowtools_wowblocks_raidprogress_save($edit);
      break;

    case WOWTOOLS_WOWBLOCKS_BLOCK_RECRUITMENT:
      _wowtools_wowblocks_recruitment_save($edit);
      break;

  }
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function wowtools_wowblocks_block_view($delta = '') {
  $block['content']['#attached']['css'] = array(drupal_get_path('module', 'wowtools_wowblocks') . '/wowtools_wowblocks.css');
  switch ($delta) {
    case WOWTOOLS_WOWBLOCKS_BLOCK_RAIDPROGRESS:
      $block['subject'] = t('Progress');
      $block['content']['#markup'] = wowtools_wowblocks_raidprogress_content();
      $block['content']['#attached']['js'] = array(
        drupal_get_path('module', 'wowtools_wowblocks') . '/wowtools_wowblocks.raidprogress.js',
      );
      break;

    case WOWTOOLS_WOWBLOCKS_BLOCK_RECRUITMENT:
      $block['subject'] = t('Recruitment');
      $block['content']['#markup'] = wowtools_wowblocks_recruitment_content();
      $block['content']['#attached']['css'][] = drupal_get_path('module', 'wowtools') . '/wowtools.css';
      break;
  }
  return $block;
}

/**
 * Implements hook_cron().
 *
 * This hook gets executed when cron is running.
 */
function wowtools_wowblocks_cron() {
  // Updates the raid progress.
  if (wowtools_wowblocks_raidprogress_settings_check()) {
    watchdog('wowtools_wowblocks', 'Raid progress updated. @bosses more down.', array('@bosses' => wowtools_wowblocks_update_raid_progress_data()));
  }
}
