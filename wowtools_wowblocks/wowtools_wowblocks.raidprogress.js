/**
 * @file
 * Used for the raid progression block to hide/show addon or raid contents.
 */

(function ($) {
  Drupal.behaviors.wowblocks = {
    attach: function (context, settings) {
      $("#wow-progression .wow-progression-wrapper").hide();
      $("#wow-progression .wow-progression-wrapper .wow-progression-bosses").hide();
      $("#wow-progression div.addon-wrapper:first-child .wow-progression-wrapper").show();
      $("#wow-progression .addon-head").click(function () {
        $(this).parent().children('.addon-wrapper .wow-progression-wrapper').toggle();
      });
      $(".wow-progression-wrapper .bg-zone").click(function () {
        $(this).parent().children('.wow-progression-wrapper .wow-progression-bosses').toggle();
      });
    }
  };
})(jQuery);
