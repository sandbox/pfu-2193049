<?php

/**
 * @file
 * Updates the raid progress.
 */

require_once dirname(dirname(dirname(__FILE__))) . '/wowtools.inc';

// Current WoW expansion.
define('WOWTOOLS_WOWBLOCKS_CURRENT_ADDON', 5);

/**
 * Goes through data and updates cleared bosses.
 *
 * @param array $bosses
 *   Raid boss data.
 * @param array $status
 *   The status on boss kills.
 * @param int $raid_id
 *   The raid instance id.
 * @param string $mode
 *   The difficulty mode.
 */
function wowtools_wowblocks_progress_status(array $bosses, array &$status, $raid_id, $mode) {
  static $timestamp = 'timestamp';
  $diff_timestamp = $mode . 'Timestamp';
  for ($i = 0; $i < count($bosses); $i++) {
    // Skip if no timestamp or boss down.
    if ($bosses[$i][$diff_timestamp] == 0 || $status[$raid_id]['bosses'][$i][$mode]['down'] == TRUE) {
      continue;
    }
    // Add timestamp if not available.
    if (!in_array($bosses[$i][$diff_timestamp], $status[$raid_id]['bosses'][$i][$mode][$timestamp][$timestamp])) {
      $status[$raid_id]['bosses'][$i][$mode][$timestamp][$timestamp][] = $bosses[$i][$diff_timestamp];
      $status[$raid_id]['bosses'][$i][$mode][$timestamp]['count'][] = 1;
    }
    else {
      $key = array_search($bosses[$i][$diff_timestamp], $status[$raid_id]['bosses'][$i][$mode][$timestamp][$timestamp]);
      $status[$raid_id]['bosses'][$i][$mode][$timestamp]['count'][$key]++;
      // Check for multiple occurrences of this timestamp, mark boss as cleared.
      if ($status[$raid_id]['bosses'][$i][$mode][$timestamp]['count'][$key] >= 10) {
        $status[$raid_id]['bosses'][$i][$mode][$timestamp] = format_date($status[$raid_id]['bosses'][$i][$mode][$timestamp][$timestamp][$key], 'short');
        $status[$raid_id]['bosses'][$i][$mode]['down'] = TRUE;
        $status[$raid_id][$mode . 'ClearCount']++;
      }
    }
  }
}

/**
 * Updates the progress of a guild on the current raid content.
 */
function wowtools_wowblocks_update_raid_progress() {
  $curls = array();
  // Set up curls for guild members.
  foreach (variable_get('wowtools_wowblocks_raidprogress_update_guildmembers') as $member) {
    $curl = curl_init(wowtools_api_profile($member['name'], array('progression'), FALSE, $member['realm']));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_TIMEOUT, 5);
    $curls[] = $curl;
  }
  $running = NULL;
  $handler = curl_multi_init();
  $raid_results = array();
  $curl_count = count($curls);
  $i = 0;
  do {
    curl_multi_add_handle($handler, $curls[$i++]);
    // 10 parallel requests at most.
    while ($i < $curl_count && $i % 10 != 0) {
      curl_multi_add_handle($handler, $curls[$i++]);
    }
    do {
      curl_multi_exec($handler, $running);
    } while ($running > 0);
    $exec_time = array();
    // Fetch results from battle net api.
    while ($msg = curl_multi_info_read($handler)) {
      $exec_time[] = curl_getinfo($msg['handle'], CURLINFO_TOTAL_TIME);
      $results = wowtools_api_data_valid_curl(curl_getinfo($msg['handle'], CURLINFO_HTTP_CODE), curl_multi_getcontent($msg['handle']));
      if (isset($results['data'])) {
        // Position of current raids in API.
        static $current_raid = 31;
        // Add boss data to raid results.
        for ($j = $current_raid; $j < count($results['data']['progression']['raids']); $j++) {
          $raid_results[$results['data']['progression']['raids'][$j]['id']][] = $results['data']['progression']['raids'][$j]['bosses'];
        }
      }
      curl_multi_remove_handle($handler, $msg['handle']);
    }

    // Evaluate time spent on requests and sleep if necessary.
    $waiting_time = 1 - max($exec_time);
    if ($waiting_time > 0) {
      usleep(1000000 * $waiting_time);
    }
    usleep(100 * 1000);
  } while ($i < $curl_count);
  curl_multi_close($handler);

  $status = variable_get('wowtools_wowblocks_raidprogress_status', wowtools_wowblocks_init_progress_status());
  // Update raid progress.
  if (!empty($raid_results)) {
    foreach ($status as $raid_id => &$raid) {
      // Go through boss modes and update progress.
      for ($i = $raid['currentBossMode']['id']; $i < count($raid['bossModes']); $i++) {
        $boss_mode = $raid['bossModes'][$i];
        // Update status on bosses.
        foreach ($raid_results[$raid_id] as $result) {
          wowtools_wowblocks_progress_status($result, $status, $raid_id, $boss_mode);
        }
        // Reset timestamps.
        foreach ($raid['bosses'] as &$boss) {
          if (isset($boss[$boss_mode]['timestamp']['timestamp'])) {
            $boss[$boss_mode]['timestamp']['timestamp'] = array();
            $boss[$boss_mode]['timestamp']['count'] = array();
          }
        }
      }
      if ($raid['currentBossMode']['id'] < 2) {
        // Set higher boss mode.
        if ($raid['mythicClearCount'] > 0) {
          $raid['currentBossMode']['name'] = 'mythic';
          $raid['currentBossMode']['id'] = 2;
        }
        elseif ($raid['currentBossMode']['id'] == 0 && $raid['heroicClearCount'] > 0) {
          $raid['currentBossMode']['name'] = 'heroic';
          $raid['currentBossMode']['id'] = 1;
        }
      }
    }
  }
  return $status;
}

/**
 * Initializes status array with latest raid content.
 */
function wowtools_wowblocks_init_progress_status() {
  $raids = variable_get('wowtools_wowblocks_raidprogress_raids');
  // ID for Patch 6.2 Hellfire Citadel
  $current_raid_ids = array(7545);
  $status = array();
  foreach ($current_raid_ids as $raid) {
    $status[$raid] = array(
      'bossModes' => array_map('strtolower', $raids[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$raid]['modes']),
      'currentBossMode' => array(
        'id' => 0,
        'name' => 'normal',
      ),
      'normalClearCount' => 0,
      'heroicClearCount' => 0,
      'mythicClearCount' => 0,
      'bosses' => array(),
    );
    for ($i = 0; $i < count($raids[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$raid]['bosses']); $i++) {
      $status[$raid]['bosses'][] = array(
        'normal' => array(
          'timestamp' => array(
            'count' => array(),
            'timestamp' => array(),
          ),
          'down' => FALSE,
        ),
        'heroic' => array(
          'timestamp' => array(
            'count' => array(),
            'timestamp' => array(),
          ),
          'down' => FALSE,
        ),
        'mythic' => array(
          'timestamp' => array(
            'count' => array(),
            'timestamp' => array(),
          ),
          'down' => FALSE,
        ),
      );
    }
  }
  return $status;
}
