<?php

/**
 * @file
 * Configuration for the raid progress block, contents of the block.
 */

/**
 * The raid progress form.
 */
function wowtools_wowblocks_raidprogress_configure(&$form) {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
  );
  $form['settings']['update'] = array(
    '#type' => 'checkbox',
    '#default_value' => wowtools_wowblocks_raidprogress_settings_check(),
    '#title' => t('Automatically update latest raid content (!guild-settings-url)', array('!guild-settings-url' => l(t('Guild Settings'), 'admin/config/wowtools/settings'))),
    '#description' => t('Updates are performed with every cron run.'),
    '#disabled' => !wowtools_settings_made(),
  );
  $settings = variable_get('wowtools_wowblocks_raidprogress_update_members');
  $form['settings']['ranks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Guild Members'),
    '#states' => array(
      'visible' => array(
        '#edit-settings-update' => array(
          'checked' => TRUE,
        ),
      ),
    ),
    '#description' => t('Select at least 10 members to update the raid progress correctly. (Data from @last-update-time)', array('@last-update-time' => format_date(variable_get('wowtools_guild_last_update'), 'short'))),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  foreach (variable_get('wowtools_guild_members', array()) as $member_rank_id => $member_rank) {
    $form['settings']['ranks']['members'][$member_rank_id]['select'] = array(
      '#type' => 'select',
      '#options' => array(
        0 => NULL,
        1 => t('All'),
        2 => t('Specific'),
      ),
      '#empty_option' => '-Rank' . $member_rank_id . '-',
      '#empty_value' => 0,
      '#default_value' => isset($settings[$member_rank_id]['select']) ? $settings[$member_rank_id]['select'] : 0,
    );
    $form['settings']['ranks']['members'][$member_rank_id]['member'] = array(
      '#type' => 'fieldset',
      '#states' => array(
        'visible' => array(
          "#edit-settings-ranks-members-{$member_rank_id}-select" => array(
            'value' => 2,
          ),
        ),
      ),
      '#tree' => TRUE,
    );
    $options = array();
    $i = 1;
    foreach ($member_rank as $member) {
      $options[$i] = array(
        'id' => $i,
        'name' => $member['name'],
      );
      $i++;
    }
    $form['settings']['ranks']['members'][$member_rank_id]['member']['table'] = array(
      '#type' => 'tableselect',
      '#header' => array(
        'id' => '#',
        'name' => t('Name'),
      ),
      '#options' => $options,
      '#empty' => t('No max level characters found'),
      '#js_select' => FALSE,
      '#default_value' => isset($settings[$member_rank_id]['members']) ? $settings[$member_rank_id]['members'] : array(1),
    );
  }
  $form['settings']['actions'] = array(
    '#type' => 'actions',
    '#states' => array(
      'visible' => array(
        "#edit-settings-update" => array(
          'checked' => TRUE,
        ),
      ),
    ),
    '#suffix' => '<div id="' . drupal_html_id('wowtools-wowblocks-raidupdate-message') . '"></div>',
  );
  $form['settings']['actions']['update_progress'] = array(
    '#type' => 'button',
    '#value' => t('Submit and Update'),
    '#ajax' => array(
      'callback' => '_wowtools_wowblocks_raidprogress_settings_update_ajax',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('updating..'),
      ),
      'effect' => 'fade',
    ),
    '#executes_submit_callback' => TRUE,
    '#submit' => array(
      '_wowtools_wowblocks_raidprogress_submit',
    ),
  );
  $form['settings']['actions']['update_members'] = array(
    '#type' => 'submit',
    '#value' => t('Update Members'),
    '#submit' => array('_wowtools_wowblocks_raidprogress_settings_update_members_submit'),
  );
  $addon_checks = variable_get('wowtools_wowblocks_raidprogress_addon_checks', FALSE);
  $zone_checks = variable_get('wowtools_wowblocks_raidprogress_zone_checks', 1);
  $boss_checks = variable_get('wowtools_wowblocks_raidprogress_boss_checks');
  $raids = variable_get('wowtools_wowblocks_raidprogress_raids');
  $expansions = variable_get('wowtools_wowblocks_raidprogress_expansions');
  $form['progress'] = array(
    '#type' => 'fieldset',
    '#title' => t('Raids'),
    '#tree' => TRUE,
  );
  foreach (array_reverse($raids, TRUE) as $addon_id => $addon) {
    $form['progress']['raids'][$addon_id] = array(
      '#type' => 'fieldset',
      '#title' => $expansions[$addon_id],
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => isset($addon_checks[$addon_id]) ? $addon_checks[$addon_id] : TRUE,
    );
    foreach (array_reverse($addon, TRUE) as $zone_id => $zone) {
      $form['progress']['raids'][$addon_id][$zone_id]['check'] = array(
        '#type' => 'checkbox',
        '#default_value' => isset($zone_checks[$addon_id][$zone_id]) ? $zone_checks[$addon_id][$zone_id] : 0,
        '#title' => $zone['name'],
      );
      $form['progress']['raids'][$addon_id][$zone_id]['bosses'] = array(
        '#type' => 'fieldset',
        '#title' => 'Encounter',
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#states' => array(
          'visible' => array(
            "#edit-progress-raids-{$addon_id}-{$zone_id}-check" => array(
              'checked' => TRUE,
            ),
          ),
        ),
      );
      $form['progress']['raids'][$addon_id][$zone_id]['bosses']['raid_mode'] = array(
        '#type' => 'radios',
        '#default_value' => isset($boss_checks[$addon_id][$zone_id]['raid_mode']) ? $boss_checks[$addon_id][$zone_id]['raid_mode'] : 0,
        '#title' => t('Raid Difficulty'),
        '#options' => $zone['modes'],
      );
      $options = array();
      foreach ($zone['bosses'] as $boss_id => $boss) {
        $id = $boss_id + 1;
        $options[$id] = array(
          'id' => $id,
          'name' => $boss,
        );
      }
      $form['progress']['raids'][$addon_id][$zone_id]['bosses']['table'] = array(
        '#type' => 'tableselect',
        '#header' => array(
          'id' => '#',
          'name' => t('BOSS'),
        ),
        '#options' => $options,
        '#default_value' => isset($boss_checks[$addon_id][$zone_id]['bosses']) ? $boss_checks[$addon_id][$zone_id]['bosses'] : array(),
      );
    }
  }
}

/**
 * Check settings for the automatic raid progress updater.
 *
 * @return bool
 *   TRUE if settings available, FALSE otherwise.
 */
function wowtools_wowblocks_raidprogress_settings_check() {
  $settings_made = wowtools_settings_made();
  if ($settings_made) {
    // Reset all raid progress data if not up-to-date.
    if (variable_get('wowtools_wowblocks_raidprogress_guild_last_update', 0) < variable_get('wowtools_guild_last_update')) {
      variable_set('wowtools_wowblocks_raidprogress_guild_last_update', variable_get('wowtools_guild_last_update'));
      variable_set('wowtools_wowblocks_raidprogress_update', FALSE);
      variable_set('wowtools_wowblocks_raidprogress_update_members', NULL);
      variable_set('wowtools_wowblocks_raidprogress_update_guildmembers', NULL);
      variable_set('wowtools_wowblocks_raidprogress_status', NULL);
    }
  }
  return $settings_made && variable_get('wowtools_wowblocks_raidprogress_update', FALSE);
}

/**
 * Used to update the raid data and reflect it to the view.
 *
 * @return int
 *   The number of new bosses down.
 */
function wowtools_wowblocks_update_raid_progress_data() {
  module_load_include('inc', 'wowtools_wowblocks', 'includes/wowtools_wowblocks.raidprogress');
  $status = wowtools_wowblocks_update_raid_progress();

  // Get all variables.
  $addon_checks = variable_get('wowtools_wowblocks_raidprogress_addon_checks');
  $zone_checks = variable_get('wowtools_wowblocks_raidprogress_zone_checks');
  $boss_checks = variable_get('wowtools_wowblocks_raidprogress_boss_checks');
  $view = variable_get('wowtools_wowblocks_raidprogress_view', array());

  // Update data to view.
  $check_clear = TRUE;
  foreach ($status as $zone_id => $zone) {
    $boss_mode = $zone['currentBossMode']['name'];
    $boss_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id]['bosses'] = array();
    foreach ($zone['bosses'] as $boss_id => $boss) {
      if ($boss[$boss_mode]['down'] == TRUE) {
        $boss_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id]['bosses'][$boss_id + 1] = strval($boss_id + 1);
      }
    }
    if ($zone_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id] == FALSE) {
      $zone_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id] = TRUE;
      if (isset($view[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON])) {
        array_unshift($view[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON], $zone_id);
      }
      else {
        $view[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][] = $zone_id;
      }
    }
    $boss_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id]['raid_mode'] = $zone['currentBossMode']['id'];
    $boss_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON][$zone_id]['clear_count'] = $zone[$boss_mode . 'ClearCount'];
    if ($check_clear == TRUE && $zone['mythicClearCount'] < count($zone['bosses'])) {
      $check_clear = FALSE;
    }
  }
  $addon_checks[WOWTOOLS_WOWBLOCKS_CURRENT_ADDON] = FALSE;
  if ($check_clear == TRUE) {
    variable_set('wowtools_wowblocks_raidprogress_guild_last_update', 0);
  }

  // Set count of new bosses down.
  $boss_clear_count = 0;
  $old_status = variable_get('wowtools_wowblocks_raidprogress_status', wowtools_wowblocks_init_progress_status());
  foreach ($old_status as $zone_id => $zone) {
    $clear_mode = $status[$zone_id]['currentBossMode']['name'] . 'ClearCount';
    $boss_clear_count += $status[$zone_id][$clear_mode] - $zone[$clear_mode];
  }

  // Set all variables.
  variable_set('wowtools_wowblocks_raidprogress_status', $status);
  variable_set('wowtools_wowblocks_raidprogress_addon_checks', $addon_checks);
  variable_set('wowtools_wowblocks_raidprogress_zone_checks', $zone_checks);
  variable_set('wowtools_wowblocks_raidprogress_boss_checks', $boss_checks);
  variable_set('wowtools_wowblocks_raidprogress_view', $view);

  return $boss_clear_count;
}

/**
 * Button submit to update the guild member data.
 */
function _wowtools_wowblocks_raidprogress_settings_update_members_submit($form, &$form_state) {
  if (!wowtools_settings_made()) {
    wowtools_settings_error();
  }
  else {
    module_load_include('inc', 'wowtools', 'wowtools');
    $region = variable_get('wowtools_guild_region');
    $server = variable_get('wowtools_guild_server');
    $guild_name = variable_get('wowtools_guild_name');
    $guild = wowtools_api_data(wowtools_api_profile($guild_name, array('members'), TRUE, $server, $region));
    if (isset($guild['data'])) {
      wowtools_api_guildmembers($guild['data']);
      variable_set('wowtools_guild_last_update', REQUEST_TIME);
      drupal_set_message(t('Guild Members updated.'));
    }
    else {
      drupal_set_message(t(check_plain($guild['err_msg'])), 'error');
    }
  }
}

/**
 * Render API callback: Updates the raid progress.
 *
 * Sets the selected ranks and updates the raid progress on this
 * ranks.
 *
 * This function is assigned as an #ajax callback in
 * wowtools_wowblocks_raidprogress_configure()
 */
function _wowtools_wowblocks_raidprogress_settings_update_ajax($form, $form_state) {
  $commands = array();
  if (!wowtools_settings_made()) {
    wowtools_settings_error();
    //module_load_include('js', 'wowtools');
    $commands[] = array('command' => 'refreshCurrentPage');
  }
  elseif ($form_state['values']['settings']['update'] == TRUE) {
    $guild_members = variable_get('wowtools_guild_members');
    $members_update = array();
    $settings = array();
    $valid_count = 0;
    foreach ($form_state['values']['settings']['ranks']['members'] as $rank_id => $rank) {
      $settings[$rank_id]['select'] = $rank['select'];
      if ($rank['select'] > 0) {
        // Check all members.
        if ($rank['select'] == 1) {
          foreach ($guild_members[$rank_id] as $guild_member) {
            $members_update[] = $guild_member;
            $valid_count++;
          }
        }
        else {
          $settings[$rank_id]['members'] = array_filter($rank['member']['table']);
          if (!empty($settings[$rank_id]['members'])) {
            foreach ($settings[$rank_id]['members'] as $chosen) {
              $members_update[] = $guild_members[$rank_id][$chosen - 1];
              $valid_count++;
            }
          }
          else {
            $settings[$rank_id]['select'] = 0;
          }
        }
      }
    }
    if ($valid_count < 10) {
      drupal_set_message(t('You have to select at least 10 members.'), 'error');
    }
    elseif ($valid_count > 100) {
      drupal_set_message(t('You can only select 100 members at most.'), 'error');
    }
    else {
      variable_set('wowtools_wowblocks_raidprogress_update_members', $settings);
      variable_set('wowtools_wowblocks_raidprogress_update_guildmembers', $members_update);
      variable_set('wowtools_wowblocks_raidprogress_update', $form_state['values']['settings']['update']);
      drupal_set_message(t('Raid progress updated. @bosses more down.', array('@bosses' => wowtools_wowblocks_update_raid_progress_data())));
      //module_load_include('js', 'wowtools');
      $commands[] = array('command' => 'refreshCurrentPage');
    }
  }
  $commands[] = ajax_command_replace('#wowtools-wowblocks-raidupdate-message', '<div id="' . drupal_html_id('wowtools-wowblocks-raidupdate-message') . '">' . theme('status_messages') . '</div>');
  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Render API callback: Saves the updated raidprogress.
 *
 * Saves the data which is triggered by another call.
 *
 * This function is assigned as an #submit callback in
 * wowtools_wowblocks_raidprogress_configure()
 */
function _wowtools_wowblocks_raidprogress_submit($form, &$form_state) {
  $edit['progress']['raids'] = $form_state['values']['progress']['raids'];
  $edit['settings']['update'] = $form_state['values']['settings']['update'];
  _wowtools_wowblocks_raidprogress_save($edit);
}

/**
 * Saves all input from raid progress form.
 *
 * @param array $edit
 *   Contains all fields to save.
 */
function _wowtools_wowblocks_raidprogress_save(array $edit) {
  $view = array();
  $addon_checks = array();
  $zone_checks = array();
  $boss_checks = array();
  foreach ($edit['progress']['raids'] as $addon_id => $addon) {
    foreach ($addon as $zone_id => $zone) {
      if ($zone['check'] == TRUE) {
        $boss_checks[$addon_id][$zone_id]['bosses'] = array_filter($zone['bosses']['table']);
        $boss_checks[$addon_id][$zone_id]['raid_mode'] = $zone['bosses']['raid_mode'];
        $boss_checks[$addon_id][$zone_id]['clear_count'] = count($boss_checks[$addon_id][$zone_id]['bosses']);
        $view[$addon_id][] = $zone_id;
      }
      else {
        $boss_checks[$addon_id][$zone_id]['bosses'] = array();
        $boss_checks[$addon_id][$zone_id]['raid_mode'] = 'Normal';
      }
      $zone_checks[$addon_id][$zone_id] = $zone['check'];
    }
    $addon_checks[$addon_id] = !in_array(TRUE, $zone_checks[$addon_id]);
  }
  if ($edit['settings']['update'] == FALSE && (variable_get('wowtools_wowblocks_raidprogress_update', FALSE) == TRUE)) {
    variable_set('wowtools_wowblocks_raidprogress_guild_last_update', 0);
    variable_set('wowtools_wowblocks_raidprogress_update', $edit['settings']['update']);
  }
  variable_set('wowtools_wowblocks_raidprogress_addon_checks', $addon_checks);
  variable_set('wowtools_wowblocks_raidprogress_zone_checks', $zone_checks);
  variable_set('wowtools_wowblocks_raidprogress_boss_checks', $boss_checks);
  variable_set('wowtools_wowblocks_raidprogress_view', $view);
}

/**
 * Contents of the block.
 *
 * @return string
 *   The html contents.
 */
function wowtools_wowblocks_raidprogress_content() {
  $raids = variable_get('wowtools_wowblocks_raidprogress_raids');
  $boss_checks = variable_get('wowtools_wowblocks_raidprogress_boss_checks');
  $view = variable_get('wowtools_wowblocks_raidprogress_view', array());
  $img_path = base_path() . drupal_get_path('module', 'wowtools_wowblocks');
  $output = '<div class="wowtools" id="' . drupal_html_id('wow-progression') . '">';
  foreach ($view as $addon_id => $addon) {
    $output .= '<div class="addon-wrapper" id="' . drupal_html_id('addon-' . $addon_id) . '"><div style="background: url(' . $img_path . '/images/raidprogress/' . $addon_id . '.png) no-repeat center" class="addon-head"></div>';
    foreach ($addon as $zone_id) {
      $raid_mode_title = $raids[$addon_id][$zone_id]['modes'][$boss_checks[$addon_id][$zone_id]['raid_mode']];
      $output .= '<div class="wow-progression-wrapper">';
      $output .= '<table style="background-color: #444;" class="bg-zone"><tr><td><h3>' . $raids[$addon_id][$zone_id]['name'] . '</h3><span>' . $boss_checks[$addon_id][$zone_id]['clear_count'] . ' / ' . $raids[$addon_id][$zone_id]['boss_count'] . ' ' . $raid_mode_title . '</span></td></tr></table>';
      $output .= '<table id="' . drupal_html_id($zone_id . '-bosses') . '" class="wow-progression-bosses">';
      foreach ($raids[$addon_id][$zone_id]['bosses'] as $boss_id => $boss) {
        $output .= isset($boss_checks[$addon_id][$zone_id]['bosses'][$boss_id + 1]) ? '<tr class="complete">' : '<tr class="noncomplete">';
        $output .= '<td class="boss-text">' . $raids[$addon_id][$zone_id]['bosses'][$boss_id] . '</td>';
        $output .= '<td class="diff-icon">';
        $output .= theme('image', array(
          'path' => $img_path . '/images/icon-completed.png',
          'alt' => $raid_mode_title,
          'title' => $raid_mode_title,
          'width' => '14',
          'height' => '14',
          'attributes' => NULL,
        ));
        $output .= '</td>';
        $output .= '</tr>';
      }
      $output .= '</table>';
      $output .= '</div>';
    }
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}
