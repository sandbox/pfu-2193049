<?php

/**
 * @file
 * Configuration for the recruitment block, contents of the block.
 */

/**
 * The recruitment form.
 */
function wowtools_wowblocks_recruitment_configure(&$form) {
  $rec_form = variable_get('wowtools_wowblocks_recruitment_form');
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
  );
  $form['settings']['all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Recruit all classes'),
    '#default_value' => $rec_form['recruit_all'],
  );
  $form['settings']['apply']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Apply link'),
    '#default_value' => $rec_form['apply_link']['enabled'],
  );
  $form['settings']['apply']['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Url text'),
    '#default_value' => $rec_form['apply_link']['text'],
    '#states' => array(
      'visible' => array(
        "#edit-settings-apply-enabled" => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );
  $form['settings']['apply']['link'] = array(
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => $rec_form['apply_link']['link'],
    '#states' => array(
      'visible' => array(
        "#edit-settings-apply-enabled" => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );
  $form['settings']['hide_closed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide class specs not recruiting'),
    '#default_value' => $rec_form['hide_closed'],
  );
  $form['classes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Classes'),
    '#tree' => TRUE,
  );
  foreach (variable_get('wowtools_classes') as $class_id => $class) {
    $form['classes'][$class_id] = array(
      '#type' => 'fieldset',
      '#title' => $class['name'],
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => $rec_form['classes'][$class_id]['check'],
    );
    // Class roles.
    foreach ($class['roles'] as $key => $role) {
      $form['classes'][$class_id][$key]['role'] = array(
        '#type' => 'select',
        '#title' => $role,
        '#options' => array(
          'closed' => t('closed'),
          'open' => t('open'),
          'low' => t('low'),
          'medium' => t('medium'),
          'high' => t('high'),
        ),
        '#default_value' => $rec_form['classes'][$class_id]['roles'][$key]['role'],
      );
    }
  }
}

/**
 * Saves all input from recruitment form.
 *
 * @param array $edit
 *   Contains all fields to save.
 */
function _wowtools_wowblocks_recruitment_save(array $edit) {
  $view = variable_get('wowtools_wowblocks_recruitment_view');
  $rec_form = variable_get('wowtools_wowblocks_recruitment_form');
  $rec_form['hide_closed'] = $edit['settings']['hide_closed'];
  $rec_form['recruit_all'] = $edit['settings']['all'];
  $rec_form['apply_link']['enabled'] = $edit['settings']['apply']['enabled'];

  // Saves the apply fields, if apply enabled.
  if ($rec_form['apply_link']['enabled'] == TRUE) {
    $rec_form['apply_link']['text'] = $edit['settings']['apply']['text'];
    $rec_form['apply_link']['link'] = $edit['settings']['apply']['link'];
    $view['apply_link']['text'] = $edit['settings']['apply']['text'];
    $view['apply_link']['link'] = $edit['settings']['apply']['link'];
  }
  else {
    $view['apply_link'] = NULL;
  }

  $view['classes'] = array();

  // Restores all classes if 'recruit all' checked.
  if ($rec_form['recruit_all'] == TRUE) {
    $view['select'] = 'all';
    foreach ($rec_form['classes'] as &$class) {
      $class['check'] = TRUE;
      foreach ($class['roles'] as &$role) {
        $role['role'] = 'closed';
      }
    }
  }

  // Sets the selected classes.
  else {
    $classes = variable_get('wowtools_classes');
    foreach ($edit['classes'] as $class_id => $class) {
      $rec_form['classes'][$class_id]['check'] = TRUE;
      foreach ($class as $i => $role) {
        // Sets the class as enabled.
        if ($role['role'] !== 'closed') {
          $rec_form['classes'][$class_id]['check'] = FALSE;
          $view['classes'][$class_id]['roles'][$i]['prio'] = $role['role'];
          $view['classes'][$class_id]['roles'][$i]['role'] = $classes[$class_id]['roles'][$i];
        }
        // Enables classes not recruiting.
        elseif ($rec_form['hide_closed'] == FALSE) {
          $view['classes'][$class_id]['roles'][$i]['prio'] = $role['role'];
          $view['classes'][$class_id]['roles'][$i]['role'] = $classes[$class_id]['roles'][$i];
        }
        $rec_form['classes'][$class_id]['roles'][$i]['role'] = $role['role'];
      }
      // If all class specs are stated 'closed'.
      if ($rec_form['classes'][$class_id]['check'] == TRUE) {
        if ($rec_form['hide_closed'] == FALSE) {
          $view['classes'][$class_id]['name'] = $classes[$class_id]['name'];
          $view['classes'][$class_id]['prio'] = 1;
        }
      }
      else {
        $view['classes'][$class_id]['name'] = $classes[$class_id]['name'];
        $view['classes'][$class_id]['prio'] = count($classes[$class_id]['roles']);
      }
    }
    if (empty($view['classes'])) {
      $view['select'] = 'closed';
    }
  }
  variable_set('wowtools_wowblocks_recruitment_form', $rec_form);
  variable_set('wowtools_wowblocks_recruitment_view', $view);
}

/**
 * Contents of the block.
 *
 * @return string
 *   The html contents.
 */
function wowtools_wowblocks_recruitment_content() {
  $view = variable_get('wowtools_wowblocks_recruitment_view');
  $output = '<div class="wowtools" id="' . drupal_html_id('wow-recruitment') . '">';
  $output .= '<ul>';
  if (!empty($view['classes'])) {
    $img_path = base_path() . drupal_get_path('module', 'wowtools');
    $role_tags = variable_get('wowtools_role_tags');
    foreach ($view['classes'] as $key => $class) {
      $class_img = theme('image', array(
        'path' => 'http://us.media.blizzard.com/wow/icons/18/class_' . $key . '.jpg',
        'width' => '20',
        'height' => '20',
        'attributes' => NULL,
      ));
      if ($class['prio'] == 1) {
        $output .= '<li class="class-rec">';
        $output .= $class_img;
        $output .= '<span class="class-rec-info">';
        $output .= '<span class="wowclass wowclass-' . $key . '">' . $class['name'] . '</span>';
        $output .= '<span class="prio prio-role prio-' . $class['roles'][0]['prio'] . '">' . $class['roles'][0]['prio'] . '</span>';
        $output .= '</span></li>';
      }
      else {
        $output .= '<li class="class-rec"><a class="class-rec-specs">';
        $output .= $class_img;
        $output .= '<span class="class-rec-info wowclass wowclass-' . $key . '">' . $class['name'] . '</span></a>';
        $output .= '<ul class="class-role">';
        foreach ($class['roles'] as $role) {
          $output .= '<li class="class-rec">';
          $output .= '<span class="class-roles"><span class="class-role-icon class-role-' . $role_tags[$role['role']] . '"></span></span>';
          $output .= '<span class="class-rec-info class-sub-info">';
          $output .= '<span class="wowclass wowclass-' . $key . '">' . $role['role'] . '</span>';
          $output .= '<span class="prio prio-' . $role['prio'] . '">' . $role['prio'] . '</span></li>';
          $output .= '</span>';
        }
        $output .= '</ul></li>';
      }
    }
  }
  else {
    $output .= '<li class="settings"><span>' . $view[$view['select']] . '</span></li>';
  }

  if (isset($view['apply_link'])) {
    $output .= '<li class="apply"><span>' . l(t($view['apply_link']['text']), $view['apply_link']['link']) . '</span></li>';
  }
  $output .= '</ul></div>';
  return $output;
}
