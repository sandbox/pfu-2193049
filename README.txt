CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Data file
 * Customization
 * FAQ


INTRODUCTION
------------

The WoWTools suite provides various tools for World of Warcraft including a
block to recruit new classes for your guild and also a block to show the
current raid progress you guild is on.

For a full description of the module, visit the project page:
  http://www.drupal.org/sandbox/pfu/2193049

To submit bug reports and feature suggestions, or to track changes:
  http://www.drupal.org/project/issues/2193049
 

REQUIREMENTS
------------

The WoWBlocks module requires the WoWTools module which is included.


INSTALLATION
------------

* Install as usual, see 
  http://www.drupal.org/documentation/install/modules-themes/modules-7 for
  further information.


CONFIGURATION
-------------

* WoWTools module
  As base for other modules in the WoWTools suite you can configure some
  World of Wacraft specific settings which are not required, but they enable
  some additional features.

  - Configure user permissions in Administration » People » Permissions:

    * Administer world of warcraft tools
      To have access to the settings form this permission has to be granted.

  - Configure wowtools settings in Administration » Configuration under
    WOWTOOLS:

    * Settings

      - API Settings
        The API KEY is needed to be able to fetch data from the official
        Battle Net API. Just register at https://dev.battle.net/member/register
        and register the application to get your own key.
      
      - Guild Information
        Here you can fill in your guild information like name, region and
        server. When you submit the information the member data is retrieved
        from Battle Net and stored.


* WoWBlocks module
  Includes a recruitment and raid progress block.

  - Recruitment block
    In Administration » Structure » WoWBlocks: Recruitment you can
    configure this block.
  
    Above the regular block settings you can find a form to configure the
    recruitments. Just press the standard save button below to save the
    settings from this form.
  
    - Settings

      * Recruit all classes
        This means your block shows just a text to recruit all classes instead
        of showing all classes. If you want to recruit specific classes again
        you have to uncheck this checkbox.
      
      * Apply link
        This enables a link to your apply page for your guild. Fill in
        your custom url and url text.

      * Hide class specs not recruiting
        By default this option is enabled. If you want to show all classes even
        those you don't recruit, you need to uncheck this.
    
    - Classes

      Here you can select all classes you want to recruit. For every single
      class you can choose the specs individually and select open/closed or
      the priority you need this spec low/middle/high.

  - Raid progress block
    In your Administration » Structure » WoWBlocks: Raid Progress you can
    configure this block.

    Above the regular block settings you can find a form to configure the
    raidprogress. Just press the standard save button below to save the
    settings from this form.

    - Settings

      * Automatically update latest raid content
        This feature requires you to set up your guild information.

        - Guild Members
          Here you can select guild members to update the raid
          progress of your guild. Once submitted, the raid progress gets
          updated while cron is running. If your member data is outdated, you
          can update your guild members if you need to by pressing the button
          below.

    - Raids
    
      Includes all the raid instances since the third addon Cataclysm.
      Select the raids you want to show, set the raid difficulty and enable
      the encounters your raid has defeated.


DATA FILE
---------

The wowtools.data.json file includes all necessary data from World
of Warcraft to use for all modules in the WoWTools suite. The data is quite
static, if there are changes to the game this file will be updated. Currently
there is only the raid data which has to be updated every few months.
      
      
CUSTOMIZATION
-------------

The files responsible for the styles are:

* WoWTools CSS file
  <your_module_folder>/wowtools/wowtools.css

  This file includes only a few things. You can change the font used
  for WoWTools & WoWBlocks module and also the wow class colors.
  
* WoWBlocks CSS file
  <your_module_folder>/wowtools/wowtools_wowblocks/wowtools_wowblocks.css

  Here you can make some more adjustments if you like. This includes the raid
  progress & recruitment block.


FAQ
---

Q: The updated raid progress data is wrong, why?

A: Just a few notes how the update works. The updater goes through your checked
   guild members and collects the timestamps of every bosskill. If there have at 
   least 10 people killed one boss at the same time, the boss will be marked as 
   completed. Sometimes the data is wrong because there are less then 10 people 
   showing up in the data (propably players left the guild).

Q: Why do I have to select members to update the raid progress?

A: Since the Battle Net API has changed and can only execute 10 requests per
   second at once, it's necessary to limit them. If I would request all
   member data of a guild (also large guilds 600+ members) it would take much
   longer, then just 100 requests.
